import { useState } from "react";
import { fetchData } from "./apiService";
import { QueryParams, ApiResponse } from "./types";
import { useSearchParams } from "react-router-dom";
import { ColumnOrderState } from "@tanstack/react-table";
import { useTableContext } from "./context";

const useTable = () => {
  const [data, setData] = useState<ApiResponse>({ data: [], totalRows: 0 });
  const [loading, setLoading] = useState<boolean>(false);
  const [searchParams, setSearchParams] = useSearchParams();

  const [columnOrder] = useState<ColumnOrderState>([]);

  const DEFAULT_PAGE_SIZE = [20, 40, 100, 200];

  const getApiPagination = () => {
    const pageIndex = parseInt(searchParams.get("page") || "0", 10);
    const pageSize = parseInt(
      searchParams.get("pageSize") || DEFAULT_PAGE_SIZE[0].toString(),
      10
    );
    return { pageIndex, pageSize };
  };

  const getTablePagination = (queryParams: QueryParams) => {
    return {
      page: queryParams.pageIndex.toString(),
      pageSize: queryParams.pageSize.toString(),
      filter: queryParams.filter || "",
    };
  };

  const updateSearchParams = (tablePaginationFn: any) => {
    const currentPag = getApiPagination();
    const value = tablePaginationFn?.(currentPag);

    // TODO: If changes pageSize, returns to page zero

    setSearchParams(getTablePagination(value));
    fetchTableData(value);
  };

  const fetchTableData = async (fetchParams?: QueryParams) => {
    setLoading(true);
    const paginationInfo = fetchParams ?? getApiPagination();
    try {
      const response = await fetchData(paginationInfo);
      setData(response);
    } catch (error) {
      console.error("Error fetching table data:", error);
    } finally {
      setLoading(false);
    }
  };

  return {
    constants: {
      DEFAULT_PAGE_SIZE,
    },
    data,
    loading,
    fetchTableData,
    pagination: getApiPagination(),
    onPaginationChange: updateSearchParams,
    columnOrder,
  };
};

export default useTable;

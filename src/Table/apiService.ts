import { QueryParams, ApiResponse } from "./types";

const fetchData = async (queryParams: QueryParams): Promise<ApiResponse> => {
  const { pageSize, pageIndex } = queryParams;
  const url = new URL("https://api.punkapi.com/v2/beers");

  // Assuming the API supports pagination parameters, adjust as needed
  url.searchParams.append("per_page", pageSize.toString());
  url.searchParams.append("page", (pageIndex + 1).toString()); // Punk API pages are 1-indexed

  const response = await fetch(url.toString());
  const data = await response.json();

  // You need to adjust this based on the actual API response structure
  return { data, totalRows: 350, pageIndex, pageSize };
};

export { fetchData };

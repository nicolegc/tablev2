import { createContext, useContext } from "react";
import { useReactTable } from "@tanstack/react-table";
import { UserConfig, TableData } from "./types";

// Context interface
interface TableContextProps {
  instance: ReturnType<typeof useReactTable<TableData>>;
  userConfig: UserConfig;
  tableConfig: any;
}

export const TableContext = createContext<TableContextProps | undefined>(
  undefined
);

export const useTableContext = () => {
  const context = useContext(TableContext);
  if (!context) {
    throw new Error("useTableContext must be used within a TableComponent");
  }
  return context;
};

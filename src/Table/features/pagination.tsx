import { FC } from "react";
import { useTableContext } from "../context";

const getPageSizeOptions = (
  { pagination = {} }: any = {},
  defaultOptions: number[]
): number[] => {
  if (pagination?.pageOptions) return pagination.pageOptions;
  return defaultOptions;
};

const Pagination: FC = () => {
  const { instance, tableConfig, userConfig } = useTableContext();
  const pageSizeOptions = getPageSizeOptions(
    userConfig,
    tableConfig.constants.DEFAULT_PAGE_SIZE
  );

  // TODO: Allow user to sen de Page Selector they want
  return (
    <div className="w-full flex items-center justify-between gap-4 p-4 bg-white border-t border-gray-200">
      {/* Page size selector */}
      <div>
        <span className="text-sm text-gray-700">Show </span>
        <select
          value={tableConfig.pagination.pageSize}
          onChange={(e) => instance.setPageSize(Number(e.target.value))}
          className="p-1 text-sm text-gray-700 bg-white border border-gray-300 rounded focus:ring-blue-500 focus:border-blue-500"
        >
          {pageSizeOptions.map((size) => (
            <option key={size} value={size}>
              {size}
            </option>
          ))}
        </select>
        <span className="text-sm text-gray-700"> entries</span>
      </div>

      {/* Pagination buttons */}
      <div className="flex items-center gap-1">
        <button
          onClick={() => instance.setPageIndex(0)}
          disabled={!instance.getCanPreviousPage()}
          className="px-3 py-1 text-sm text-gray-700 bg-gray-200 border border-gray-300 rounded hover:bg-gray-300 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50"
        >
          First
        </button>
        <button
          onClick={instance.previousPage}
          disabled={!instance.getCanPreviousPage()}
          className="px-3 py-1 text-sm text-gray-700 bg-gray-200 border border-gray-300 rounded hover:bg-gray-300 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50"
        >
          Previous
        </button>
        <span className="px-3 py-1 text-sm text-gray-700 bg-gray-200 border border-gray-300 rounded">
          Page <strong>{tableConfig.pagination.pageIndex + 1}</strong> of{" "}
          <strong>
            {Math.ceil(
              tableConfig.data.totalRows / tableConfig.pagination.pageSize
            )}
          </strong>
        </span>
        <button
          onClick={instance.nextPage}
          disabled={!instance.getCanNextPage()}
          className="px-3 py-1 text-sm text-gray-700 bg-gray-200 border border-gray-300 rounded hover:bg-gray-300 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50"
        >
          Next
        </button>
        <button
          onClick={() => instance.setPageIndex(instance.getPageCount() - 1)}
          disabled={!instance.getCanNextPage()}
          className="px-3 py-1 text-sm text-gray-700 bg-gray-200 border border-gray-300 rounded hover:bg-gray-300 focus:outline-none focus:ring focus:ring-blue-500 focus:ring-opacity-50"
        >
          Last
        </button>
      </div>
    </div>
  );
};

export default Pagination;

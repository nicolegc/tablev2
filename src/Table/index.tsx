import React, { useEffect, useMemo } from "react";
import {
  ColumnDef,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { UserConfig, TableData } from "./types";
import useTable from "./useTable";
import { TableContext } from "./context";
import TableHeader from "./components/Header";
import DefaultBody from "./components/Body";
import Pagination from "./features/pagination";

const TableComponent: React.FC<UserConfig> = (userConfig) => {
  const tableConfig = useTable();

  const columns = useMemo<ColumnDef<TableData, any>[]>(
    () => [
      {
        accessorKey: "name", // Accessor is the "key" in the data
        header: "Name", // Header is the label for the column
      },
      {
        accessorKey: "tagline",
        header: "Tagline",
      },
      {
        accessorKey: "description",
        header: "Description",
      },
    ],
    []
  );

  const {
    data,
    pagination,
    columnOrder,
    fetchTableData,
    onPaginationChange,
  } = tableConfig;

  useEffect(() => {
    fetchTableData();
  }, []);

  const table = useReactTable({
    data: tableConfig.data.data,
    columns: columns,
    manualSorting: true,
    manualPagination: true,
    pageCount: Math.ceil(data.totalRows / pagination.pageSize),
    state: { pagination, columnOrder },
    getCoreRowModel: getCoreRowModel(),
    onPaginationChange,
    // onColumnOrderChange,
  });

  return (
    <TableContext.Provider value={{ instance: table, userConfig, tableConfig }}>
      {/* Table Component */}
      <table className="relative w-full min-w-full divide-y h-25">
        <TableHeader />
        <DefaultBody />
      </table>
      <Pagination />

      {/* Table Component */}
    </TableContext.Provider>
  );
};

export default TableComponent;

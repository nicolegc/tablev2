export interface TableData {
  [key: string]: any;
}

interface PageParams {
  pageSize: number;
  pageIndex: number;
}

export interface QueryParams extends PageParams {
  filter?: string;
}

type Partial<T> = {
  [P in keyof T]?: T[P];
};
export interface ApiResponse extends Partial<PageParams> {
  data: TableData[];
  totalRows: number;
}

export interface UserConfig {
  columns: any[];
  [key: string]: any;
}

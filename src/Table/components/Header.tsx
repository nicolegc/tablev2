import { flexRender } from "@tanstack/react-table";
import { useTableContext } from "../context";

interface HeaderSort {
  isSorted: boolean | "asc" | "desc";
}

const HeaderSortIcon = ({ isSorted }: HeaderSort) => {
  if (isSorted) {
    return isSorted === "desc" ? (
      <svg
        className="w-4 h-4 ml-2 inline-block"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
      >
        <path
          fillRule="evenodd"
          d="M5.293 14.707a1 1 0 001.414 0L10 11.414l3.293 3.293a1 1 0 101.414-1.414l-4-4a1 1 0 00-1.414 0l-4 4a1 1 0 000 1.414z"
          clipRule="evenodd"
        />
      </svg>
    ) : (
      <svg
        className="w-4 h-4 ml-2 inline-block"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
      >
        <path
          fillRule="evenodd"
          d="M14.707 5.293a1 1 0 00-1.414 0L10 8.586 6.707 5.293a1 1 0 00-1.414 1.414l4 4a1 1 0 001.414 0l4-4a1 1 0 000-1.414z"
          clipRule="evenodd"
        />
      </svg>
    );
  }
  return null;
};

const TableHeader = () => {
  const { instance } = useTableContext();

  return (
    <thead className="w-100 text-xs text-white uppercase bg-gradient-to-r from-indigo-500 to-purple-600 sticky top-0 z-10">
      {instance.getHeaderGroups().map((headerGroup) => (
        <tr>
          {headerGroup.headers.map((column) => (
            <th
              key={column.id}
              className="sticky px-6 py-3 text-left text-xs font-semibold uppercase tracking-wider cursor-pointer hover:bg-blue-700"
            >
              {flexRender(column.column.columnDef.header, column.getContext())}
              <HeaderSortIcon isSorted={column.column.getIsSorted()} />
            </th>
          ))}
        </tr>
      ))}
    </thead>
  );
};

export default TableHeader;

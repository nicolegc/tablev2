import { flexRender } from "@tanstack/react-table";
import { useTableContext } from "../../context";

const DefaultBody = () => {
  const { instance } = useTableContext();

  return (
    <tbody className="bg-white divide-y divide-gray-200">
      {instance.getRowModel().rows.map((row: any) => {
        return (
          <tr key={row.id} className="hover:bg-gray-100">
            {row.getVisibleCells().map((cell: any) => {
              return (
                <td
                  key={cell.id}
                  className="p-3 text-sm font-normal text-gray-700 text-left"
                >
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              );
            })}
          </tr>
        );
      })}
    </tbody>
  );
};

export default DefaultBody;

import { BrowserRouter } from "react-router-dom";
import "./App.css";
import TableComponent from "./Table";

function App() {
  return (
    <BrowserRouter>
      <TableComponent />
    </BrowserRouter>
  );
}

export default App;
